\documentclass[a4paper]{article}
\usepackage[a4paper,left=3cm,right=2cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{palatino}
\usepackage[colorlinks=true,linkcolor=blue,citecolor=blue]{hyperref}
\usepackage{graphicx}
\usepackage{cp1819t}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{color}
%================= local x=====================================================%
\def\getGif#1{\includegraphics[width=0.3\textwidth]{cp1819t_media/#1.png}}
\let\uk=\emph
\def\aspas#1{``#1"}
%================= lhs2tex=====================================================%
%include polycode.fmt
%format (div (x)(y)) = x "\div " y
%format succ = "\succ "
%format ==> = "\Longrightarrow "
%format map = "\map "
%format length = "\length "
%format fst = "\p1"
%format p1  = "\p1"
%format snd = "\p2"
%format p2  = "\p2"
%format Left = "i_1"
%format Right = "i_2"
%format i1 = "i_1"
%format i2 = "i_2"
%format >< = "\times"
%format >|<  = "\bowtie "
%format |-> = "\mapsto"
%format . = "\comp "
%format (kcomp (f)(g)) = f "\kcomp " g
%format -|- = "+"
%format conc = "\mathsf{conc}"
%format summation = "{\sum}"
%format (either (a) (b)) = "\alt{" a "}{" b "}"
%format (frac (a) (b)) = "\frac{" a "}{" b "}"
%format (uncurry f) = "\uncurry{" f "}"
%format (const f) = "\underline{" f "}"
%format TLTree = "\mathsf{TLTree}"
%format (lcbr (x)(y)) = "\begin{lcbr}" x "\\" y "\end{lcbr}"
%format (split (x) (y)) = "\conj{" x "}{" y "}"
%format (for (f) (i)) = "\for{" f "}\ {" i "}"
%format B_tree = "\mathsf{B}\mbox{-}\mathsf{tree} "
\def\ana#1{\mathopen{[\!(}#1\mathclose{)\!]}}
%format (cataA (f) (g)) = "\cata{" f "~" g "}_A"
%format (anaA (f) (g)) = "\ana{" f "~" g "}_A"
%format (cataB (f) (g)) = "\cata{" f "~" g "}_B"
%format (anaB (f) (g)) = "\ana{" f "~" g "}_B"
%format Either a b = a "+" b
%format fmap = "\mathsf{fmap}"
%format NA   = "\textsc{na}"
%format NB   = "\textsc{nb}"
%format inT = "\mathsf{in}"
%format outT = "\mathsf{out}"
%format Null = "1"
%format (Prod (a) (b)) = a >< b
%format fF = "\fun F "
%format e1 = "e_1 "
%format e2 = "e_2 "
%format Dist = "\fun{Dist}"
%format IO = "\fun{IO}"
%format BTree = "\fun{BTree} "
%format LTree = "\mathsf{LTree}"
%format inNat = "\mathsf{in}"
%format inFS = "\mathsf{in}"
%format outFS = "\mathsf{out}"
%format (cataNat (g)) = "\cata{" g "}"
%format (cataFS (g)) = "\cata{" g "}"
%format Nat0 = "\N_0"
%format muB = "\mu "
%format (frac (n)(m)) = "\frac{" n "}{" m "}"
%format (fac (n)) = "{" n "!}"
%format (underbrace (t) (p)) = "\underbrace{" t "}_{" p "}"
%format matrix = "matrix"
%format (bin (n) (k)) = "\Big(\vcenter{\xymatrix@R=1pt{" n "\\" k "}}\Big)"
%format `ominus` = "\mathbin{\ominus}"
%format % = "\mathbin{/}"
%format <-> = "{\,\leftrightarrow\,}"
%format <|> = "{\,\updownarrow\,}"
%format `minusNat`= "\mathbin{-}"
%format ==> = "\Rightarrow"
%format .==>. = "\Rightarrow"
%format .<==>. = "\Leftrightarrow"
%format .==. = "\equiv"
%format .<=. = "\leq"
%format .&&&. = "\wedge"
%format cdots = "\cdots "
%format pi = "\pi "
%format exp(a)(b) =  a "^ {" b "}"
%format frac a b = "$$\frac{" a "}{" b"}$$"

%---------------------------------------------------------------------------

\title{
       	    Cálculo de Programas
\\
       	Trabalho Prático
\\
       	MiEI+LCC --- 2018/19
}

\author{
       	\dium
\\
       	Universidade do Minho
}


\date\mydate

\makeindex
\newcommand{\rn}[1]{\textcolor{red}{#1}}
\begin{document}

\maketitle

\begin{center}\large
\begin{tabular}{ll}
\textbf{Grupo} nr. & 79
\\\hline
a76434 & Nelson Miguel de Oliveira Estevão
\\
a83712 & Rui Filipe Moreira Mendes
\\
a85493 & Pedro Alexandre Gonçalves Ribeiro
\end{tabular}
\end{center}

\section{Preâmbulo}

A disciplina de \CP\ tem como objectivo principal ensinar
a progra\-mação de computadores como uma disciplina científica. Para isso
parte-se de um repertório de \emph{combinadores} que formam uma álgebra da
programação (conjunto de leis universais e seus corolários) e usam-se esses
combinadores para construir programas \emph{composicionalmente}, isto é,
agregando programas já existentes.

Na sequência pedagógica dos planos de estudo dos dois cursos que têm
esta disciplina, restringe-se a aplicação deste método à programação
funcional em \Haskell. Assim, o presente trabalho prático coloca os
alunos perante problemas concretos que deverão ser implementados em
\Haskell.  Há ainda um outro objectivo: o de ensinar a documentar
programas, validá-los, e a produzir textos técnico-científicos de
qualidade.

\section{Documentação} Para cumprir de forma integrada os objectivos
enunciados acima vamos recorrer a uma técnica de programa\-ção dita
``\litp{literária}'' \cite{Kn92}, cujo princípio base é o seguinte:
\begin{quote}\em Um programa e a sua documentação devem coincidir.
\end{quote} Por outras palavras, o código fonte e a documentação de um
programa deverão estar no mesmo ficheiro.

O ficheiro \texttt{cp1819t.pdf} que está a ler é já um exemplo de \litp{programação
literária}: foi gerado a partir do texto fonte \texttt{cp1819t.lhs}\footnote{O
suffixo `lhs' quer dizer \emph{\lhaskell{literate Haskell}}.} que encontrará
no \MaterialPedagogico\ desta disciplina descompactando o ficheiro \texttt{cp1819t.zip}
e executando
\begin{Verbatim}[fontsize=\small]
    $ lhs2TeX cp1819t.lhs > cp1819t.tex
    $ pdflatex cp1819t
\end{Verbatim}
em que \href{https://hackage.haskell.org/package/lhs2tex}{\texttt\LhsToTeX} é
um pre-processador que faz ``pretty printing''
de código Haskell em \Latex\ e que deve desde já instalar executando
\begin{Verbatim}[fontsize=\small]
    $ cabal install lhs2tex
\end{Verbatim}
Por outro lado, o mesmo ficheiro \texttt{cp1819t.lhs} é executável e contém
o ``kit'' básico, escrito em \Haskell, para realizar o trabalho. Basta executar
\begin{Verbatim}[fontsize=\small]
    $ ghci cp1819t.lhs
\end{Verbatim}

%if False
\begin{code}
{-# OPTIONS_GHC -XNPlusKPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving, DeriveDataTypeable, FlexibleInstances #-}
module CP1819t where
import CP
import List  hiding (fac)
import Nat
import Data.List hiding (find)
import Data.Typeable
import Data.Ratio
import Data.Bifunctor
import Data.Maybe
import Data.Matrix hiding ((!))
import Test.QuickCheck hiding ((><),choose)
import qualified Test.QuickCheck as QuickCheck
import System.Random  hiding (split)
import System.Process
import GHC.IO.Exception
import qualified Graphics.Gloss as G
import Control.Monad
import Control.Applicative hiding ((<|>))
import Data.Either
import Data.Char
import Exp
\end{code}
%endif

\noindent Abra o ficheiro \texttt{cp1819t.lhs} no seu editor de texto preferido
e verifique que assim é: todo o texto que se encontra dentro do ambiente
\begin{quote}\small\tt
\verb!\begin{code}!
\\ ... \\
\verb!\end{code}!
\end{quote}
vai ser seleccionado pelo \GHCi\ para ser executado.

\section{Como realizar o trabalho}
Este trabalho teórico-prático deve ser realizado por grupos de três alunos.
Os detalhes da avaliação (datas para submissão do relatório e sua defesa
oral) são os que forem publicados na \cp{página da disciplina} na \emph{internet}.

Recomenda-se uma abordagem participativa dos membros do grupo
de trabalho por forma a poderem responder às questões que serão colocadas
na \emph{defesa oral} do relatório.

Em que consiste, então, o \emph{relatório} a que se refere o parágrafo anterior?
É a edição do texto que está a ser lido, preenchendo o anexo \ref{sec:resolucao}
com as respostas. O relatório deverá conter ainda a identificação dos membros
do grupo de trabalho, no local respectivo da folha de rosto.

Para gerar o PDF integral do relatório deve-se ainda correr os comando seguintes,
que actualizam a bibliografia (com \Bibtex) e o índice remissivo (com \Makeindex),
\begin{Verbatim}[fontsize=\small]
    $ bibtex cp1819t.aux
    $ makeindex cp1819t.idx
\end{Verbatim}
e recompilar o texto como acima se indicou. Dever-se-á ainda instalar o utilitário
\QuickCheck,
que ajuda a validar programas em \Haskell\ e a biblioteca \gloss{Gloss} para
geração de gráficos 2D:
\begin{Verbatim}[fontsize=\small]
    $ cabal install QuickCheck gloss
\end{Verbatim}
Para testar uma propriedade \QuickCheck~|prop|, basta invocá-la com o comando:
\begin{verbatim}
    > quickCheck prop
    +++ OK, passed 100 tests.
\end{verbatim}

Qualquer programador tem, na vida real, de ler e analisar (muito!) código
escrito por outros. No anexo \ref{sec:codigo} disponibiliza-se algum
código \Haskell\ relativo aos problemas que se seguem. Esse anexo deverá
ser consultado e analisado à medida que isso for necessário.

\Problema

Um compilador é um programa que traduz uma linguagem dita de
\emph{alto nível} numa linguagem (dita de \emph{baixo nível}) que
seja executável por uma máquina.
Por exemplo, o \gcc{GCC} compila C/C++ em código objecto que
corre numa variedade de arquitecturas.

Compiladores são normalmente programas complexos.
Constam essencialmente de duas partes:
o \emph{analisador sintático} que lê o texto de entrada
(o programa \emph{fonte} a compilar) e cria uma sua representação
interna, estruturada em árvore;
e o \emph{gerador de código} que converte essa representação interna
em código executável.
Note-se que tal representação intermédia pode ser usada para outros fins,
por exemplo,
para gerar uma listagem de qualidade (\uk{pretty print}) do programa fonte.

O projecto de compiladores é um assunto complexo que
será assunto de outras disciplinas.
Neste trabalho pretende-se apenas fazer uma introdução ao assunto,
mostrando como tais programas se podem construir funcionalmente à custa de
cata/ana/hilo-morfismos da linguagem em causa.

Para cumprirmos o nosso objectivo, a linguagem desta questão terá que ser, naturalmente,
muito simples: escolheu-se a das expressões aritméticas com inteiros,
\eg\ \( 1 + 2 \), \( 3 * (4 + 5) \) etc.
Como representação interna adopta-se o seguinte tipo polinomial, igualmente simples:
%
\begin{spec}
data Expr = Num Int | Bop Expr Op Expr
data Op = Op String
\end{spec}

\begin{enumerate}
\item
Escreva as definições dos \{cata, ana e hilo\}-morfismos deste tipo de dados
segundo o método ensinado nesta disciplina (recorde módulos como \eg\ |BTree| etc).
\item
Como aplicação do módulo desenvolvido no ponto 1,
defina como \{cata, ana ou hilo\}-morfismo a função seguinte:
\begin{itemize}
\item |calcula :: Expr -> Int| que calcula o valor
de uma expressão;
\begin{propriedade}
O valor zero é um elemento neutro da adição.
\begin{code}
prop_neutro1 :: Expr -> Bool
prop_neutro1 = calcula . addZero .==. calcula where
            addZero e = Bop (Num 0) (Op "+") e
prop_neutro2 :: Expr -> Bool
prop_neutro2 = calcula . addZero .==. calcula where
            addZero e = Bop e (Op "+") (Num 0) 
\end{code}
\end{propriedade}  
\begin{propriedade}
As operações de soma e multiplicação são comutativas.
\begin{code}
prop_comuta = calcula . mirror .==. calcula where
            mirror = cataExpr (either Num g2)
            g2 = (uncurry (uncurry Bop)) . (swap >< id) . assocl . (id >< swap)
\end{code}
\end{propriedade}  
\end{itemize}
\item
Defina como \{cata, ana ou hilo\}-morfismos as funções
\begin{itemize}
\item |compile :: String -> Codigo| - trata-se do compilador propriamente
      dito. Deverá ser gerado código posfixo para uma máquina elementar
      de \pda{stack}. O tipo |Codigo| pode ser definido à escolha.
      Dão-se a seguir exemplos de comportamentos aceitáveis para esta
      função:
\begin{verbatim}
Tp4> compile "2+4"
["PUSH 2", "PUSH 4", "ADD"]
Tp4> compile "3*(2+4)"
["PUSH 3", "PUSH 2", "PUSH 4", "ADD", "MUL"]
Tp4> compile "(3*2)+4"
["PUSH 3", "PUSH 2", "MUL", "PUSH 4", "ADD"]
Tp4> 
\end{verbatim}
\item |show' :: Expr -> String| - gera a representação textual
      de uma |Expr| pode encarar-se como o \uk{pretty printer}
      associado ao nosso compilador
\begin{propriedade}
Em anexo, é fornecido o código da função |readExp|, que é ``inversa" da função |show'|,
tal como a propriedade seguinte descreve:
\begin{code}
prop_inv :: Expr -> Bool
prop_inv = p1 . head . readExp . show' .==. id
\end{code}
\end{propriedade}  
\end{itemize}
%% \item Generalize o tipo |Expr| de forma a admitir operadores
%% unários (\eg\ \(-5\)) e repita os exercícios dos pontos anteriores.
\end{enumerate}

\paragraph{Valorização}
Em anexo é apresentado código \Haskell\ que permite declarar
|Expr| como instância da classe |Read|. Neste contexto,  
|read| pode ser vista como o analisador
sintático do nosso minúsculo compilador de expressões aritméticas.

Analise o código apresentado, corra-o e escreva no seu relatório uma explicação
\textbf{breve} do seu funcionamento, que deverá saber defender aquando da
apresentação oral do relatório.

Exprima ainda o analisador sintático |readExp| como um anamorfismo.

\Problema

Pretende-se neste problema definir uma linguagem gráfica \aspas{brinquedo}
a duas dimensões (2D) capaz de especificar e desenhar agregações de
caixas que contêm informação textual.
Vamos designar essa linguagem por |L2D| e vamos defini-la como um tipo
 em \Haskell:
\begin{code}
type L2D = X Caixa Tipo 
\end{code}
onde |X| é a estrutura de dados
\begin{code}
data X a b = Unid a | Comp b (X a b) (X a b) deriving Show
\end{code}
e onde:
\begin{code}
type Caixa = ((Int,Int),(Texto,G.Color))
type Texto = String
\end{code}
Assim, cada caixa de texto é especificada pela sua largura, altura, o seu
texto e a sua côr.\footnote{Pode relacionar |Caixa| com as caixas de texto usadas
nos jornais ou com \uk{frames} da linguagem \Html\ usada na Internet.}
Por exemplo,
\begin{spec}
((200,200),("Caixa azul",col_blue))
\end{spec}
designa a caixa da esquerda da figura \ref{fig:L2D}.

O que a linguagem |L2D| faz é agregar tais caixas tipográficas
umas com as outras segundo padrões especificados por vários
\aspas{tipos}, a saber,
\begin{code}
data Tipo = V | Vd | Ve | H | Ht | Hb 
\end{code}
com o seguinte significado:
\begin{itemize}
\item[|V|] - agregação vertical alinhada ao centro
\item[|Vd|] - agregação vertical justificada à direita
\item[|Ve|] - agregação vertical justificada à esquerda
\item[|H|] - agregação horizontal alinhada ao centro
\item[|Hb|] - agregação horizontal alinhada pela base
\item[|Ht|] - agregação horizontal alinhada pelo topo
\end{itemize}
Como |L2D| instancia o parâmetro |b| de |X| com
|Tipo|, é fácil de ver que cada \aspas{frase} da linguagem
|L2D| é representada por uma árvore binária em que cada nó
indica qual o tipo de agregação a aplicar às suas duas sub-árvores.
%
Por exemplo, a frase
\begin{code}
ex2= Comp Hb  (Unid ((100,200),("A",col_blue)))
              (Unid ((50,50),("B",col_green)))
\end{code}
deverá corresponder à imagem da direita da figura \ref{fig:L2D}.
E poder-se-á ir tão longe quando a linguagem o permita. Por exemplo, pense na
estrutura da frase que representa o \uk{layout} da figura \ref{fig:L2D1}.

\begin{figure}
\centering
\begin{picture}(190.00,130.00)(-15,-15)
\put(00.00,0.00){$(0,0)$}
\put(80.00,50.00){$(200,200)$}
\put(20.00,-10.00){
	\includegraphics[width=70\unitlength]{cp1819t_media/ex3.png}
}
\end{picture}
%
\includegraphics[width=0.2\textwidth]{cp1819t_media/ex2.png}
%
\caption{Caixa simples e caixa composta.\label{fig:L2D}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{cp1819t_media/ex.png}
\caption{\uk{Layout} feito de várias caixas coloridas.\label{fig:L2D1}}
\end{figure}

É importante notar que cada ``caixa'' não dispõe informação relativa
ao seu posicionamento final na figura. De facto, é a posição relativa
que deve ocupar face às restantes caixas que irá determinar a sua
posição final. Este é um dos objectivos deste trabalho:
\emph{calcular o posicionamento absoluto de cada uma das caixas por forma a
respeitar as restrições impostas pelas diversas agregações}. Para isso vamos
considerar um tipo de dados que comporta a informação de todas as
caixas devidamente posicionadas (i.e. com a informação adicional da
origem onde a caixa deve ser colocada).

\begin{spec}
type Fig = [(Origem,Caixa)]
type Origem = (Float, Float)
\end{spec}
%
A informação mais relevante deste tipo é a referente à lista de
``caixas posicionadas'' (tipo |(Origem,Caixa)|). Regista-se aí a origem
da caixa que, com a informação da sua altura e comprimento, permite
definir todos os seus pontos (consideramos as caixas sempre paralelas
aos eixos). 

\begin{enumerate}
\item Forneça a definição da função |calc_origems|, que calcula as
coordenadas iniciais das caixas no plano:
\begin{spec}
calc_origems :: (L2D,Origem) -> X (Caixa,Origem) ()
\end{spec}
\item Forneça agora a definição da função |agrup_caixas|, que agrupa
todas as caixas e respectivas origens numa só lista:
\begin{spec}
agrup_caixas :: X (Caixa,Origem) () -> Fig
\end{spec}
\end{enumerate}

Um segundo problema neste projecto é \emph{descobrir como visualizar a
informação gráfica calculada por |desenho|}. A nossa estratégia para
superar o problema baseia-se na biblioteca \gloss{Gloss}, que permite a geração
de gráficos 2D. Para tal disponibiliza-se a função
\begin{spec}
crCaixa :: Origem  -> Float -> Float -> String -> G.Color -> G.Picture
\end{spec}
que cria um rectângulo com base numa coordenada, um valor para a largura, um valor
para a altura, um texto que irá servir de etiqueta, e a cor pretendida.
Disponibiliza-se também a função
\begin{spec}
display :: G.Picture -> IO ()
\end{spec}
que dado um valor do tipo |G.picture| abre uma janela com esse valor desenhado. O objectivo
final deste exercício é implementar então uma função
\begin{spec}
mostra_caixas :: (L2D,Origem) -> IO ()
\end{spec}
que dada uma frase da linguagem |L2D| e coordenadas iniciais apresenta
o respectivo desenho no ecrã.
%
\textbf{Sugestão}:
Use a função |G.pictures| disponibilizada na biblioteca \gloss{Gloss}.

\Problema

Nesta disciplina estudou-se como fazer \pd{programação dinâmica} por cálculo,
recorrendo à lei de recursividade mútua.\footnote{Lei (\ref{eq:fokkinga})
em \cite{Ol18}, página \pageref{eq:fokkinga}.}

Para o caso de funções sobre os números naturais (|Nat0|, com functor |fF
X = 1 + X|) é fácil derivar-se da lei que foi estudada uma
	\emph{regra de algibeira}
	\label{pg:regra}
que se pode ensinar a programadores que não tenham estudado
\cp{Cálculo de Programas}. Apresenta-se de seguida essa regra, tomando como exemplo o
cálculo do ciclo-\textsf{for} que implementa a função de Fibonacci, recordar
o sistema

\begin{spec}
fib 0 = 1
fib(n+1) = f n

f 0 = 1
f (n+1) = fib n + f n
\end{spec}

Obter-se-á de imediato

\begin{code}
fib' = p1 . for loop init where
   loop(fib,f)=(f,fib+f)
   init = (1,1)
\end{code}

usando as regras seguintes:
\begin{itemize}
\item	O corpo do ciclo |loop| terá tantos argumentos quanto o número de funções mutuamente recursivas.
\item	Para as variáveis escolhem-se os próprios nomes das funções, pela ordem
que se achar conveniente.\footnote{Podem obviamente usar-se outros símbolos, mas numa primeiraleitura
dá jeito usarem-se tais nomes.}
\item	Para os resultados vão-se buscar as expressões respectivas, retirando a variável |n|.
\item	Em |init| coleccionam-se os resultados dos casos de base das funções, pela mesma ordem.
\end{itemize}
Mais um exemplo, envolvendo polinómios no segundo grau a $x^2 + b x + c$ em |Nat0|.
Seguindo o método estudado nas aulas\footnote{Secção 3.17 de \cite{Ol18}.},
de $f\ x = a x^2 + b x + c$ derivam-se duas funções mutuamente recursivas:
\begin{spec}
f 0 = c
f (n+1) = f n + k n

k 0 = a + b
k(n+1) = k n + 2 a
\end{spec}
Seguindo a regra acima, calcula-se de imediato a seguinte implementação, em Haskell:
\begin{code}
f' a b c = p1 . for loop init where
  loop(f,k) = (f+k,k+2*a)
  init = (c,a+b) 
\end{code}

Qual é o assunto desta questão, então? Considerem fórmula que dá a série de Taylor da
função coseno:
\begin{eqnarray*}
	cos\ x = \sum_{i=0}^\infty \frac{(-1)^i}{(2i)!} x^{2i}
\end{eqnarray*}
Pretende-se o ciclo-\textsf{for} que implementa a função 
|cos' x n| que dá o valor dessa série tomando |i| até |n| inclusivé:
\begin{spec}
cos' x = cdots . for loop init where cdots
\end{spec}
%
\textbf{Sugestão}: Começar por estudar muito bem o processo de cálculo dado
no anexo \ref{sec:recmul} para o problema (semelhante) da função exponencial.

\begin{propriedade}
Testes de que |cos' x| calcula bem o coseno de |pi| e o coseno de |pi/2|:
\begin{code}
prop_cos1 n = n >= 10 ==> abs(cos pi - cos' pi n) < 0.001
prop_cos2 n = n >= 10 ==> abs(cos (pi/2) - cos' (pi/2) n) < 0.001
\end{code}
\end{propriedade}

\paragraph{Valorização} Transliterar |cos'| para a linguagem C; compilar
e testar o código. Conseguia, por intuição apenas, chegar a esta função?

\Problema

Pretende-se nesta questão desenvolver uma biblioteca de funções para
manipular \emph{sistemas de ficheiros} genéricos.
Um sistema de ficheiros será visto como uma associação de \emph{nomes}
a ficheiros ou \emph{directorias}.
Estas últimas serão vistas como sub-sistemas de ficheiros e assim
recursivamente.
Assumindo que |a| é o tipo dos identificadores dos ficheiros e
directorias, e que |b| é o tipo do conteúdo dos ficheiros,
podemos definir um tipo indutivo de dados para representar sistemas de
ficheiros da seguinte forma:
\begin{code}
data FS a b = FS [(a,Node a b)] deriving (Eq,Show)
data Node a b = File b | Dir (FS a b) deriving (Eq,Show)
\end{code}
Um caminho (\emph{path}) neste sistema de ficheiros pode ser representado pelo
seguinte tipo de dados:
\begin{code}
type Path a = [a]
\end{code}
Assumindo estes tipos de dados, o seguinte termo
\begin{spec}
FS [("f1", File "Ola"),
    ("d1", Dir (FS [("f2", File "Ole"),
                    ("f3", File "Ole")
                   ]))
   ]
\end{spec}
representará um sistema de ficheiros em cuja raíz temos um ficheiro chamado
|f1| com conteúdo |"Ola"| e uma directoria chamada |"d1"| constituída por dois
ficheiros, um chamado |"f2"| e outro chamado |"f3"|, ambos com conteúdo |"Ole"|.
%
Neste caso, tanto o tipo dos identificadores como o tipo do conteúdo dos
ficheiros é |String|. No caso geral, o conteúdo de um ficheiro é arbitrário:
pode ser um binário, um texto, uma colecção de dados, etc.

A definição das usuais funções |inFS| e |recFS| para este tipo é a seguinte:
\begin{code}
inFS = FS . map (id >< inNode)
inNode = either File Dir

recFS f = baseFS id id f
\end{code}
Suponha que se pretende definir como um |catamorfismo| a função que
conta o número de ficheiros existentes num sistema de ficheiros. Uma
possível definição para esta função seria:
\begin{code}
conta :: FS a b -> Int
conta = cataFS (sum . map ((either (const 1) id) . snd))
\end{code}
O que é para fazer:
\begin{enumerate}
\item Definir as funções |outFS|, |baseFS|, |cataFS|, |anaFS| e |hyloFS|.

\item Apresentar, no relatório, o diagrama de |cataFS|.

\item Definir as seguintes funções para manipulação de sistemas de
  ficheiros usando, obrigatoriamente, catamorfismos, anamorfismos ou
  hilomorfismos:

  \begin{enumerate}
  \item Verificação da integridade do sistema de ficheiros (i.e. verificar
    que não existem identificadores repetidos dentro da mesma directoria). \\
    |check :: FS a b -> Bool|
  \begin{propriedade}
    A integridade de um sistema de ficheiros não depende da ordem em que os
    últimos são listados na sua directoria:
\begin{code}
prop_check :: FS String String -> Bool
prop_check = check . (cataFS (inFS . reverse)) .==. check
\end{code}
  \end{propriedade}
  \item Recolha do conteúdo de todos os ficheiros num arquivo indexado pelo \emph{path}.\\
    |tar :: FS a b -> [(Path a, b)]|
  \begin{propriedade}
    O número de ficheiros no sistema deve ser igual ao número de ficheiros
    listados pela função |tar|.
\begin{code}
prop_tar :: FS String String -> Bool
prop_tar = length . tar .==. conta
\end{code}
  \end{propriedade}
  \item Transformação de um arquivo com o conteúdo dos ficheiros
    indexado pelo \emph{path} num sistema de ficheiros.\\
    |untar :: [(Path a, b)] -> FS a b| \\
  \textbf{Sugestão}: Use a função |joinDupDirs| para juntar directorias que estejam na mesma
  pasta e que possuam o mesmo identificador.
  \begin{propriedade}
    A composição |tar . untar| preserva o número de ficheiros no sistema.
\begin{code}
prop_untar :: [(Path String, String)] -> Property
prop_untar = validPaths .==>. ((length . tar . untar) .==. length)
validPaths :: [(Path String, String)] -> Bool
validPaths = (== 0) . length . (filter (\(a,_) -> length a == 0))
\end{code}
\end{propriedade}
  \item Localização de todos os \emph{paths} onde existe um
    determinado ficheiro.\\
    |find :: a -> FS a b -> [Path a]|
  \begin{propriedade}
    A composição |tar . untar| preserva todos os ficheiros no sistema.
\begin{code}
prop_find :: String -> FS String String -> Bool
prop_find = curry $
      length . (uncurry find) .==. length . (uncurry find) . (id >< (untar . tar))
\end{code}
  \end{propriedade}
  \item Criação de um novo ficheiro num determinado \emph{path}.\\
    |new :: Path a -> b -> FS a b -> FS a b|
  \begin{propriedade}
A adição de um ficheiro não existente no sistema não origina ficheiros duplicados.
\begin{code}
prop_new :: ((Path String,String), FS String String) -> Property
prop_new = ((validPath .&&&. notDup) .&&&. (check . p2)) .==>.
      (checkFiles . (uncurry (uncurry new)))  where
      validPath = (/=0) . length . p1 . p1
      notDup = not . (uncurry elem) . (p1 >< ((fmap p1) . tar))
\end{code}
\textbf{Questão}: Supondo-se que no código acima se substitui a propriedade
|checkFiles| pela propriedade mais fraca |check|, será que a propriedade
|prop_new| ainda é válida? Justifique a sua resposta.
\end{propriedade}

\begin{propriedade}
	A listagem de ficheiros logo após uma adição nunca poderá ser menor que a listagem
	de ficheiros antes dessa mesma adição.
\begin{code}
prop_new2 :: ((Path String,String), FS String String) -> Property
prop_new2 = validPath .==>. ((length . tar . p2) .<=. (length . tar . (uncurry (uncurry new)))) where
      validPath = (/=0) . length . p1 . p1
\end{code}
  \end{propriedade}
  \item Duplicação de um ficheiro.\\
    |cp :: Path a -> Path a -> FS a b -> FS a b|
  \begin{propriedade}
    A listagem de ficheiros com um dado nome não diminui após uma duplicação.
\begin{code}
prop_cp :: ((Path String, Path String),  FS String String) -> Bool
prop_cp =   length . tar . p2 .<=. length . tar . (uncurry (uncurry cp))
\end{code}
  \end{propriedade}  
  \item Eliminação de um ficheiro.\\
    |rm :: Path a -> FS a b -> FS a b|

  \textbf{Sugestão}: Construir um anamorfismo |nav :: (Path a, FS a b) -> FS a b|
  que navegue por um sistema de ficheiros tendo como base o |path| dado como argumento.
    \begin{propriedade}
    Remover duas vezes o mesmo ficheiro tem o mesmo efeito que o remover apenas uma vez.
\begin{code}
prop_rm :: (Path String, FS String String) -> Bool
prop_rm = (uncurry rm ) . (split p1 (uncurry rm)) .==. (uncurry rm)
\end{code}
\end{propriedade}
\begin{propriedade}
Adicionar um ficheiro e de seguida remover o mesmo não origina novos ficheiros no sistema.
\begin{code}
prop_rm2 :: ((Path String,String), FS String String) -> Property
prop_rm2 = validPath  .==>. ((length . tar . (uncurry rm) . (split (p1. p1) (uncurry (uncurry new)))) 
         .<=. (length . tar . p2)) where
         validPath = (/=0) . length . p1 . p1
\end{code}
\end{propriedade}
  \end{enumerate}
\end{enumerate}

\paragraph{Valorização}

Definir uma função para visualizar em \graphviz{Graphviz}
a estrutura de um sistema de ficheiros. A Figura~\ref{ex_prob1}, por exemplo,
apresenta a estrutura de um sistema com precisamente dois ficheiros dentro
de uma directoria chamada |"d1"|.

Para realizar este exercício será necessário apenas  escrever o anamorfismo
\begin{quote}
|cFS2Exp :: (a, FS a b) -> (Exp () a)| 
\end{quote}
que converte a estrutura de um sistema de ficheiros numa árvore de expressões
descrita em \href{http://wiki.di.uminho.pt/twiki/pub/Education/CP/MaterialPedagogico/Exp.hs}{Exp.hs}.
A função |dotFS| depois tratará de passar a estrutura do sistema de ficheiros para o visualizador.
\begin{figure}
\centering
\includegraphics[scale=0.5]{cp1819t_media/fs.png}
\caption{Exemplo de um sistema de ficheiros visualizado em \graphviz{Graphviz}.}
\label{ex_prob1}
\end{figure}

%----------------- Programa, bibliotecas e código auxiliar --------------------%

\newpage

\part*{Anexos}

\appendix

\section{Como exprimir cálculos e diagramas em LaTeX/lhs2tex}
Estudar o texto fonte deste trabalho para obter o efeito:\footnote{Exemplos tirados de \cite{Ol18}.} 
\begin{eqnarray*}
\start
	|id = split f g|
%
\just\equiv{ universal property }
%
        |lcbr(
		p1 . id = f
	)(
		p2 . id = g
	)|
%
\just\equiv{ identity }
%
        |lcbr(
		p1 = f
	)(
		p2 = g
	)|
\qed
\end{eqnarray*}

Os diagramas podem ser produzidos recorrendo à \emph{package} \LaTeX\ 
\href{https://ctan.org/pkg/xymatrix}{xymatrix}, por exemplo: 
\begin{eqnarray*}
\xymatrix@@C=2cm{
    |Nat0|
           \ar[d]_-{|cataNat g|}
&
    |1 + Nat0|
           \ar[d]^{|id + (cataNat g)|}
           \ar[l]_-{|inNat|}
\\
     |B|
&
     |1 + B|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\section{Programação dinâmica por recursividade múltipla}\label{sec:recmul}
Neste anexo dão-se os detalhes da resolução do Exercício \ref{ex:exp} dos apontamentos da
disciplina\footnote{Cf.\ \cite{Ol18}, página \pageref{ex:exp}.},
onde se pretende implementar um ciclo que implemente
o cálculo da aproximação até |i=n| da função exponencial $exp\ x = e^x$
via série de Taylor:
\begin{eqnarray}
	exp\ x 
& = &
	\sum_{i=0}^{\infty} \frac {x^i} {i!}
\end{eqnarray}
Seja $e\ x\ n = \sum_{i=0}^{n} \frac {x^i} {i!}$ a função que dá essa aproximação.
É fácil de ver que |e x 0 = 1| e que $|e x (n+1)| = |e x n| + \frac {x^{n+1}} {(n+1)!}$.
Se definirmos $|h x n| = \frac {x^{n+1}} {(n+1)!}$ teremos |e x| e |h x| em recursividade
mútua. Se repetirmos o processo para |h x n| etc obteremos no total três funções nessa mesma
situação:
\begin{spec}
e x 0 = 1
e x (n+1) = h x n + e x n

h x 0 = x
h x (n+1) = x/(s n) * h x n

s 0 = 2
s (n+1) = 1 + s n
\end{spec}
Segundo a \emph{regra de algibeira} descrita na página \ref{pg:regra} deste enunciado,
ter-se-á, de imediato:
\begin{code}
e' x = prj . for loop init where
     init = (1,x,2)
     loop(e,h,s)=(h+e,x/s*h,1+s)
     prj(e,h,s) = e
\end{code}

\section{Código fornecido}\label{sec:codigo}

\subsection*{Problema 1}
Tipos:
\begin{code}
data Expr
  = Num Int
  | Bop Expr Op Expr
  deriving (Eq, Show)

data Op =
  Op String
  deriving (Eq, Show)

type Codigo = [String]
\end{code}
Functor de base:
\begin{code}
baseExpr f g = id -|- (f >< (g >< g))
\end{code}
Instâncias:
\begin{code}
instance Read Expr where
   readsPrec _ = readExp
\end{code}
Read para Exp's:
\begin{code}
readOp :: String -> [(Op,String)]
readOp input = do
                 (x,y) <- lex input
                 return ((Op x),y)

readNum :: ReadS Expr
readNum  = (map (\ (x,y) -> ((Num x), y))).reads

readBinOp :: ReadS Expr
readBinOp = (map (\ ((x,(y,z)),t) -> ((Bop x y z),t))) .
                   ((readNum `ou` (pcurvos readExp))
                    `depois` (readOp `depois` readExp))

readExp :: ReadS Expr
readExp = readBinOp `ou` (
          readNum `ou` (
          pcurvos readExp))
\end{code}
Combinadores:
\begin{code}

depois :: (ReadS a) -> (ReadS b) -> ReadS (a,b)
depois _ _ [] = []
depois r1 r2 input = [((x,y),i2) | (x,i1) <- r1 input ,
                                   (y,i2) <- r2 i1]

readSeq :: (ReadS a) -> ReadS [a]
readSeq r input
  = case (r input) of
    [] -> [([],input)]
    l -> concat (map continua l)
         where continua (a,i) = map (c a) (readSeq r i)
               c x (xs,i) = ((x:xs),i)

ou :: (ReadS a) -> (ReadS a) -> ReadS a
ou r1 r2 input = (r1 input) ++ (r2 input)

senao :: (ReadS a) -> (ReadS a) -> ReadS a
senao r1 r2 input = case (r1 input) of
                     [] -> r2 input
                     l  -> l

readConst :: String -> ReadS String
readConst c = (filter ((== c).fst)) . lex

pcurvos = parentesis '(' ')'
prectos = parentesis '[' ']'
chavetas = parentesis '{' '}'

parentesis :: Char -> Char -> (ReadS a) -> ReadS a
parentesis _ _ _ [] = []
parentesis ap pa r input
  = do
      ((_,(x,_)),c) <- ((readConst [ap]) `depois` (
                        r                `depois` (
                        readConst [pa])))           input
      return (x,c)
\end{code}

\subsection*{Problema 2}
Tipos:
\begin{code}
type Fig = [(Origem,Caixa)]
type Origem = (Float, Float)
\end{code}
``Helpers":
\begin{code}
col_blue = G.azure
col_green = darkgreen

darkgreen = G.dark (G.dark G.green)
\end{code}
Exemplos:
\begin{code}
ex1Caixas = G.display (G.InWindow "Problema 4" (400, 400) (40, 40)) G.white $
          crCaixa (0,0) 200 200 "Caixa azul" col_blue

ex2Caixas =  G.display (G.InWindow "Problema 4" (400, 400) (40, 40)) G.white $
          caixasAndOrigin2Pict ((Comp Hb bbox gbox),(0.0,0.0)) where
          bbox = Unid ((100,200),("A",col_blue))
          gbox = Unid ((50,50),("B",col_green))

ex3Caixas = G.display (G.InWindow "Problema 4" (400, 400) (40, 40)) G.white mtest where
          mtest = caixasAndOrigin2Pict $ (Comp Hb (Comp Ve bot top) (Comp Ve gbox2 ybox2), (0.0,0.0))
          bbox1 = Unid ((100,200),("A",col_blue))
          bbox2 = Unid ((150,200),("E",col_blue))
          gbox1 = Unid ((50,50),("B",col_green))
          gbox2 = Unid ((100,300),("F",col_green))
          rbox1 = Unid ((300,50),("C",G.red))
          rbox2 = Unid ((200,100),("G",G.red))
          wbox1 = Unid ((450,200),("",G.white))
          ybox1 = Unid ((100,200),("D",G.yellow))
          ybox2 = Unid ((100,300),("H",G.yellow))
          bot = Comp Hb wbox1 bbox2
          top = (Comp Ve (Comp Hb bbox1 gbox1) (Comp Hb rbox1 (Comp H ybox1 rbox2)))
\end{code}
A seguinte função cria uma caixa a partir dos seguintes parâmetros: origem,
largura, altura, etiqueta e côr de preenchimento.
\begin{code}
crCaixa :: Origem  -> Float -> Float -> String -> G.Color -> G.Picture
crCaixa (x,y) w h l c = G.Translate (x+(w/2)) (y+(h/2)) $  G.pictures [caixa, etiqueta] where
                    caixa = G.color c (G.rectangleSolid w h)
                    etiqueta = G.translate calc_trans_x calc_trans_y $
                             G.Scale calc_scale calc_scale $ G.color G.black $ G.Text l
                    calc_trans_x = (- ((fromIntegral (length l)) * calc_scale) / 2 )*base_shift_x
                    calc_trans_y = (- calc_scale / 2)*base_shift_y
                    calc_scale = bscale * (min h w)
                    bscale = 1/700
                    base_shift_y = 100
                    base_shift_x = 64
\end{code}
Função para visualizar resultados gráficos:
\begin{code}
display = G.display (G.InWindow "Problema 4" (400, 400) (40, 40)) G.white
\end{code}

\subsection*{Problema 4}
Funções para gestão de sistemas de ficheiros:
\begin{code}
concatFS = inFS . (uncurry (++)) . (outFS >< outFS)
mkdir (x,y) = FS [(x, Dir y)]
mkfile (x,y) = FS [(x, File y)]

joinDupDirs :: (Eq a) => (FS a b) -> (FS a b)
joinDupDirs  = anaFS (prepOut . (id >< proc) . prepIn) where
         prepIn = (id >< (map (id >< outFS))) . sls . (map distr) . outFS
         prepOut = (map undistr) . (uncurry (++)) . ((map i1) >< (map i2)) . (id >< (map (id >< inFS)))
         proc = concat . (map joinDup) . groupByName
         sls = split lefts rights

joinDup :: [(a,[b])] -> [(a,[b])]
joinDup = cataList (either nil g) where g = return . (split (p1 . p1) (concat . (map p2) . (uncurry (:))))

createFSfromFile :: (Path a, b) -> (FS a b)
createFSfromFile ([a],b) = mkfile(a,b)
createFSfromFile (a:as,b) = mkdir(a, createFSfromFile (as,b))
\end{code}
Funções auxiliares:
\begin{code}
checkFiles :: (Eq a) => FS a b -> Bool
checkFiles = cataFS ((uncurry (&&)) . (split f g)) where
           f = nr . (fmap p1) . lefts . (fmap distr)
           g = and . rights . (fmap p2)

groupByName :: (Eq a) => [(a,[b])] -> [[(a,[b])]]
groupByName = (groupBy (curry p)) where
            p = (uncurry (==)) . (p1 >< p1)

filterPath :: (Eq a) => Path a -> [(Path a, b)] -> [(Path a, b)]
filterPath = filter . (\p -> \(a,b) -> p == a)
\end{code}
Dados para testes:
\begin{itemize}
\item Sistema de ficheiros vazio:
\begin{code}
efs = FS []
\end{code}
\item Nível 0
\begin{code}
f1 = FS [("f1", File "hello world")]
f2 = FS [("f2", File "more content")]
f00 = concatFS (f1,f2)
f01 = concatFS (f1, mkdir("d1", efs))
f02 = mkdir("d1",efs)
\end{code}
\item Nível 1
\begin{code}
f10 = mkdir("d1", f00)
f11 = concatFS(mkdir("d1", f00), mkdir("d2", f00))
f12 = concatFS(mkdir("d1", f00), mkdir("d2", f01))
f13 = concatFS(mkdir("d1", f00), mkdir("d2", efs))
\end{code}
\item Nível 2
\begin{code}
f20 = mkdir("d1", f10)
f21 = mkdir("d1", f11)
f22 = mkdir("d1", f12)
f23 = mkdir("d1", f13)
f24 = concatFS(mkdir("d1",f10), mkdir("d2",f12))
\end{code}
\item Sistemas de ficheiros inválidos:
\begin{code}
ifs0 = concatFS (f1,f1)
ifs1 = concatFS (f1, mkdir("f1", efs))
ifs2 = mkdir("d1", ifs0)
ifs3 = mkdir("d1", ifs1)
ifs4 = concatFS(mkdir("d1",ifs1), mkdir("d2",f12))
ifs5 = concatFS(mkdir("d1",f1), mkdir("d1",f2))
ifs6 = mkdir("d1",ifs5)
ifs7 = concatFS(mkdir("d1",f02),mkdir("d1",f02))
\end{code}
\end{itemize}
Visualização em \graphviz{Graphviz}:
\begin{code}
dotFS :: FS String b -> IO ExitCode
dotFS = dotpict . bmap (const "") id . (cFS2Exp "root")
\end{code}
\def\omitted{``Automagically" generated:
\begin{code}
instance (Arbitrary a, Arbitrary b) => Arbitrary (FS a b) where
   arbitrary = sized genfs  where
             genfs 0 = liftM (inFS . (map (id >< (i1)))) QuickCheck.arbitrary
             genfs n = oneof [liftM (inFS . (map (id >< (i1)))) QuickCheck.arbitrary,
                     liftM (inFS . return . (id >< (i2))) (liftM2 (,)
                     QuickCheck.arbitrary (genfs (n - 1))),
                     liftM3 genAux QuickCheck.arbitrary (genfs (n - 1)) (genfs (n - 1))]
             genAux a x y = inFS [(a, i2 x), (a, i2 y)]

instance Arbitrary Expr where
   arbitrary = (genExpr 10)  where
             genExpr 0 = liftM (inExpr . i1) QuickCheck.arbitrary
             genExpr n = oneof [liftM (inExpr . i1) QuickCheck.arbitrary,
                       liftM (inExpr . i2 . (uncurry genAux1))
                       $ liftM2 (,)  (genExpr (n-1)) (genExpr (n-1)),
                       liftM (inExpr . i2 . (uncurry genAux2))
                       $ liftM2 (,)  (genExpr (n-1)) (genExpr (n-1))
                     ]
             genAux1 x y = (Op "+",(x,y))
             genAux2 x y = (Op "*",(x,y))
\end{code}}

\subsection*{Outras funções auxiliares}
%----------------- Outras definições auxiliares -------------------------------------------%
Lógicas:
\begin{code}
infixr 0 .==>.
(.==>.) :: (Testable prop) => (a -> Bool) -> (a -> prop) -> a -> Property
p .==>. f = \a -> p a ==> f a

infixr 0 .<==>.
(.<==>.) :: (a -> Bool) -> (a -> Bool) -> a -> Property
p .<==>. f = \a -> (p a ==> property (f a)) .&&. (f a ==> property (p a))

infixr 4 .==.
(.==.) :: Eq b => (a -> b) -> (a -> b) -> (a -> Bool)
f .==. g = \a -> f a == g a

infixr 4 .<=.
(.<=.) :: Ord b => (a -> b) -> (a -> b) -> (a -> Bool)
f .<=. g = \a -> f a <= g a

infixr 4 .&&&.
(.&&&.) :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
f .&&&. g = \a -> ((f a) && (g a))
\end{code}
Compilação e execução dentro do interpretador:\footnote{Pode ser útil em testes
envolvendo \gloss{Gloss}. Nesse caso, o teste em causa deve fazer parte de uma função
|main|.}
\begin{code}

run = do { system "ghc cp1819t" ; system "./cp1819t" }
\end{code}

%----------------- Soluções dos alunos -----------------------------------------%

\section{Soluções dos alunos}\label{sec:resolucao}
Os alunos devem colocar neste anexo as suas soluções aos exercícios propostos,
de acordo com o "layout" que se fornece. Não podem ser alterados os nomes ou
tipos das funções dadas, mas pode ser adicionado texto e/ou outras funções
auxiliares que sejam necessárias.

\subsection*{Problema 1}
\begin{code}
inExpr :: Either Int (Op,(Expr,Expr)) -> Expr
inExpr = either Num inBop
      where inBop (o, (e1, e2)) = Bop e1 o e2
\end{code}

\begin{code}
outExpr :: Expr -> Either Int (Op,(Expr,Expr))
outExpr (Num a) = Left a
outExpr (Bop e1 op e2) = Right (op, (e1, e2))
\end{code}

\begin{code}
recExpr f = baseExpr id f
\end{code}

\begin{code}
cataExpr g = g . (recExpr (cataExpr g)) . outExpr
\end{code}

\begin{code}
anaExpr g = inExpr . (recExpr (anaExpr g)) . g
\end{code}

\begin{code}
hyloExpr h g = cataExpr h . anaExpr g
\end{code}

\begin{code}
opera :: (Op, (Int, Int)) -> Int
opera (Op "+", (i1, i2)) = i1 + i2
opera (Op "-", (i1, i2)) = i1 - i2
opera (Op "*", (i1, i2)) = i1 * i2
opera (Op "/", (i1, i2)) = div i1 i2
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |Expr|
       \ar[d]_{|calcula|}
       \ar[r]_-{|outExpr|}
&
    |Int + (Op >< (Expr >< Expr))|
           \ar[d]^{|id + (id + (calcula >< calcula))|}
           \ar[l]_-{|inExpr|}
\\
     |Int|
&
     |Int + (Op >< (Int >< Int))|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
calcula :: Expr -> Int
calcula = cataExpr (either id opera)
\end{code}

\paragragh{}
\begin{code}
auxil :: (Op, (String, String)) -> String
auxil (Op "+", (i1, i2)) = "(" ++ i1 ++ " + " ++ i2 ++ ")"
auxil (Op "-", (i1, i2)) = "(" ++ i1 ++ " - " ++ i2 ++ ")"
auxil (Op "*", (i1, i2)) = "(" ++ i1 ++ " * " ++ i2 ++ ")"
auxil (Op "/", (i1, i2)) = "(" ++ i1 ++ " / " ++ i2 ++ ")"
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |Expr|
       \ar[d]_-{show'}
       \ar[r]_-{|outExpr|}
&
    |Int + (Op >< (Expr >< Expr))|
           \ar[d]^{|id + (id + (show' >< show'))|}
           \ar[l]_-{|inExpr|}
\\
     |String|
&
     |Int + (Op >< (String >< String))|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
show' :: Expr -> String
show' = cataExpr (either show auxil)
\end{code}

\begin{code}
stringToExpr :: String -> Expr
stringToExpr = p1 . head . readExp
\end{code}

\begin{code}
compileOp :: Op -> String
compileOp (Op s) = case s of
    "+" -> "ADD"
    "-" -> "SUB"
    "*" -> "MUL"
    "/" -> "DIV"
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |String|
        \ar[d]_{|stringToExpr|}
\\
    |Expr|
       \ar[d]_{|compile|}
       \ar[r]_-{|outExpr|}
&
    |Int + (Op >< (Expr >< Expr))|
           \ar[d]^{|id + (id + (compile >< compile))|}
           \ar[l]_-{|inExpr|}
\\
     |String|
&
     |Int + (Op >< (Codigo >< Codigo))|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
compile :: String -> Codigo
compile = cataExpr g . stringToExpr
    where g :: Either Int (Op, (Codigo, Codigo)) -> Codigo
          g = either  (singl . ("PUSH " ++) . show) (\(op, (c1, c2)) -> c1 ++ c2 ++ (singl $ compileOp op))
\end{code}

\subsection*{Valorização}

\begin{code}
getOp :: Expr -> Op
getOp (Bop ex1 op ex2) = op
\end{code}

\begin{code}
fromNum :: Expr -> Int
fromNum (Num c) = c
\end{code}

\begin{code}
myReadOp :: String -> Op
myReadOp = getOp . fst . head . readBinOp
\end{code}

\begin{code}
operadores::[Char]
operadores = ['+','*','/','-']

priorityAux [] dentroP foundOperador l = l
priorityAux (h:t) dentroP foundOperador (x,y) = if (dentroP) then
    if (h == ')') then priorityAux t False foundOperador (x++[h],y) else priorityAux t dentroP foundOperador (x++[h],y)
    else if (foundOperador) then priorityAux t dentroP foundOperador (x,y++[h])
    else if (elem h operadores) then priorityAux t dentroP True (x,y)
    else priorityAux t dentroP foundOperador (x++[h],y)

setOperationPriority l = priorityAux l True False ([],[])
\end{code}

\begin{code}
getStrings :: String -> (String, String)
getStrings s = if (head s == '(') then setOperationPriority s
               else (fst (head (lex s)), tail (snd (head (lex s))))

checkNumber :: String -> Bool
checkNumber [] = True
checkNumber (x:xs) = if ((isDigit x) == False) then False
                     else checkNumber xs

cutParenthesis :: String -> String
cutParenthesis [] = []
cutParenthesis (x : xs) = if (x /= ')') && (x /= '(') then x : cutParenthesis xs
                          else cutParenthesis xs

containsParenthesis :: String -> Bool
containsParenthesis [] = False
containsParenthesis [x] = False
containsParenthesis s = (head s == '(') || (last s == ')')

rearrangeStrings :: (String, String) -> (String, String)
rearrangeStrings (s, l) | containsParenthesis s && containsParenthesis l = (a, b)
                        | containsParenthesis s && (not (containsParenthesis l)) = (a, l)
                        | (not (containsParenthesis s)) && containsParenthesis l = (s, b)
                        | otherwise = (s, l)
                        where (a, b) = (cutParenthesis s, cutParenthesis l)

realCheckNumber :: String -> Bool
realCheckNumber s = checkNumber a && (length (snd $ head $ lex s)) == 0
    where a = fst $ head $ lex s
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |Expr|
           \ar[r]_-{|outExpr|}
&
    |Int + (Op >< (Expr >< Expr))|
           \ar[l]_-{|inExpr|}
\\
     |String|
        \ar[u]^-{|myReadExp|}
        \ar[r]^-{|g|}
&
     |Int + (Op >< (String >< String))|
        \ar[u]_-{|id + (id >< (myReadExp >< myReadExp))|}
}
\end{eqnarray*}
\begin{code}
myReadExp :: String -> Expr
myReadExp s = (anaExpr g) s
    where g :: String -> Either Int (Op, (String, String))
          g s = if (realCheckNumber s) then i1 ((fromNum . fst . head . readNum) s)
                else i2 ((myReadOp >< (rearrangeStrings . getStrings)) (s, s))
\end{code}

\subsection*{Problema 2}

\begin{code}
inL2D :: Either a (b, (X a b, X a b)) -> X a b
inL2D = either Unid inComp
  where
    inComp (b, (x1, x2)) = Comp b x1 x2

outL2D :: X a b -> Either a (b, (X a b, X a b))
outL2D (Unid c) = Left c
outL2D (Comp d (x1) (x2)) = Right (d, (x1, x2))

baseL2D f g = id -|- (f >< (g >< g))

recL2D f = baseL2D id f

cataL2D g = g . (recL2D (cataL2D g)) . outL2D

anaL2D g = inL2D . (recL2D (anaL2D g)) . g
\end{code}

\begin{code}
collectLeafs = cataL2D (either singl (conc . p2))
\end{code}

\begin{code}
getDimensaoUnid :: Caixa -> (Float, Float)
getDimensaoUnid ((a, b), (texto, cor)) = (fromIntegral a, fromIntegral b)

getDimensionJunction ::
     (Tipo, ((Float, Float), (Float, Float))) -> (Float, Float)
getDimensionJunction (V, ((a, b), (c, d))) = (max a c, b + d)
getDimensionJunction (Vd, ((a, b), (c, d))) = (max a c, b + d)
getDimensionJunction (Ve, ((a, b), (c, d))) = (max a c, b + d)
getDimensionJunction (H, ((a, b), (c, d))) = (a + c, max b d)
getDimensionJunction (Hb, ((a, b), (c, d))) = (a + c, max b d)
getDimensionJunction (Ht, ((a, b), (c, d))) = (a + c, max b d)
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |X Caixa Tipo|
       \ar[d]_{|dimen|}
       \ar[r]_-{|outL2D|}
&
    |Caixa + (Tipo, (X Caixa Tipo, X Caixa Tipo))|
           \ar[d]^{|id + (id + (dimen >< dimen))|}
           \ar[l]_-{|inL2D|}
\\
     |(Float, Float)|
&
     |Caixa + (Tipo, ((Float, Float), (Float, Float)))|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
dimen :: X Caixa Tipo -> (Float, Float)
dimen = cataL2D (either getDimensaoUnid getDimensionJunction)
\end{code}

\begin{eqnarray*}
\xymatrix@@C=0.1cm{
    |X (Caixa, Origem) ()|
           \ar[r]_-{|outExpr|}
&
    |(Caixa, Origem) + ((), (X (Caixa, Origem) (), X (Caixa, Origem) ()))|
           \ar[l]_-{|inExpr|}
\\
     |(X Caixa Tipo, Origem)|
        \ar[u]^-{|calcOrigins|}
        \ar[r]^-{|g|}
&
     |(Caixa, Origem) + ((), ((X Caixa Tipo, Origem), (X Caixa Tipo, Origem)))|
        \ar[u]_-{|id + (id >< (calcOrigins >< calcOrigins))|}
}
\end{eqnarray*}

\begin{code}
calcOrigins :: (X Caixa Tipo, Origem) -> X (Caixa, Origem) ()
calcOrigins = anaL2D g
  where
    g :: (X Caixa Tipo, Origem)
      -> Either (Caixa, Origem) ((), ((X Caixa Tipo, Origem), (X Caixa Tipo, Origem)))
    g (Unid a, o) = i1 (a, o)
    g (Comp t x1 x2, o) = i2 ((), ((x1, o), (x2, calc t o (dimen x1))))

calc :: Tipo -> Origem -> (Float, Float) -> Origem
calc t (x, y) (i, j) =
  case t of
    V  -> (x + i / 2, y + j)
    Vd -> (x + i, y + i)
    Ve -> (x, y + j)
    H  -> (x + i, y + j / 2)
    Hb -> (x + i, y)
    Ht -> (x + i, y + j)
\end{code}

\begin{eqnarray*}
\xymatrix@@C=1cm{
    |X (Caixa, Origem) ()|
       \ar[d]_{|agrupCaixas|}
       \ar[r]_-{|outL2D|}
&
    |(Caixa, Origem) + ((), (X (Caixa, Origem) (), X (Caixa, Origem) ()))|
           \ar[d]^{|id + (id + (agrupCaixas >< agrupCaixas))|}
           \ar[l]_-{|inL2D|}
\\
     |(Fig)|
&
     |(Caixa, Origem) + ((), (Fig, Fig))|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
agrupCaixas :: X (Caixa, Origem) () -> Fig
agrupCaixas = cataL2D g
  where
    g :: Either (Caixa, Origem) ((), (Fig, Fig)) -> Fig
    g (Left p) = singl $ swap p
    g (Right (_, (f1, f2))) = f1 ++ f2

criaCaixa :: (Origem, Caixa) -> G.Picture
criaCaixa (o, ((a, b), (texto, cor))) = crCaixa o (fromIntegral a) (fromIntegral b) texto cor

caixasAndOrigin2Pict = G.Pictures . map criaCaixa . agrupCaixas . calcOrigins

mostra_caixas :: (L2D, Origem) -> IO ()
mostra_caixas = display . caixasAndOrigin2Pict
\end{code}

\subsection*{Problema 3}
Solução: \\\\
Pretende implementar-se um ciclo que efetue
o cálculo da aproximação até |i=n| da função cosseno $cos\ x$
via série de Taylor:
\begin{eqnarray*}
	cos\ x = \sum_{i=0}^\infty \frac{(-1)^i}{(2i)!} x^{2i}
\end{eqnarray*}
Seja $c\ x\ n = \sum_{i=0}^{n} \frac{(-1)^i}{(2i)!} x^{2i}$ a função que dá essa aproximação,
é fácil de ver que |c x 0 = 1| e que $|c x (n+1)| = |c x n| * (-1)^{n+1} \frac {x^{2(n+1)}} {(2(n+1))!}$.
Se definirmos $|h x n| = (-1)^{n+1} \frac {x^{2(n+1)}} {(2(n+1))!}$ teremos |c x| e |h x| em recursividade
mútua. Se repetirmos o processo para |h x n| obteremos no total quatro funções nessa mesma situação:

\begin{spec}
c x 0 = 1
c x (n+1) = c x n * h x n

h x 0 = - frac (exp x 2) 2
h x (n+1) = h x n * - frac (exp x 2) (s n * t n)

s 0 = 3
s (n+1) = 2 + s n

t 0 = 4
t (n+1) = 2 + t n
\end{spec}

Segundo a \emph{regra de algibeira} descrita na página \ref{pg:regra} deste enunciado,
ter-se-á, de imediato:

\begin{code}
cos' x = prj . for loop init where
   init = (1, -x^2 / 2, 3, 4)
   loop (c, h, s, t) = (c + h, h * (-x^2) / (s * t), 2 + s, 2 + t)
   prj (c, h, s, t) = c
\end{code}

\subsection*{Valorização}
\begin{verbatim}
define P 3.14159265358979323846

double t(double n) {
    if(n == 0) return 4;
    else return 2 + t(n-1);
}

double s(double n) {
    if(n == 0) return 3;
    else return 2 + s(n - 1);
}


double h(double x, int iterations) {
    if(iterations == 0) return -(x * x) / 2;
    else return h(x, iterations - 1) *
        (-(x * x / (s(iterations - 1) * t(iterations - 1))));
}


double c(double x, int iterations) {
    if(iterations == 0) return 1;
    else return c(x, iterations - 1) + h(x, iterations - 1);
}

void loop(double *acumuladores, double arg, int iterations) {
    acumuladores[3] = t(acumuladores[3]);
    acumuladores[2] = s(acumuladores[2]);
    acumuladores[1] = h(arg, iterations);
    acumuladores[0] = c(arg, iterations);
}

double reduceAngle(double arg) {
    double redundant = arg / (2 * P);
    double toRemove = floor(redundant);
    double newAngle = 0;
    if(arg > 0) newAngle = arg - (toRemove * (2 * P));
    else newAngle = arg + (toRemove * (2 * P));
    return newAngle;
}

int main(int argc, char **argv) {
    double arg = (double) atof(argv[1]);
    arg = reduceAngle(arg);
    double accumulators[4];
    accumulators[0] = 1;
    accumulators[1] = -(arg * arg) / 2;
    accumulators[2] = 3;
    accumulators[3] = 4;
    loop(accumulators, arg, 10);
    printf("This is the result: %f\n", accumulators[0]);
}
\end{verbatim}

\subsection*{Problema 4}

Triologia ``ana-cata-hilo":

\begin{code}
outFS :: FS a b -> [(a, Either b (FS a b))]
outFS (FS l) = map (id >< outNode) l

outNode :: Node a b -> Either b (FS a b)
outNode (File a) = i1 a
outNode (Dir a) = i2 a

baseFS f g h = f >< (g -|- h)
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |FS A B|
       \ar[d]_{|cataFS g|}
&
    |[A >< (B + (FS A B))]|
           \ar[d]^{|map (id >< (id + (cataNat g)))|}
           \ar[l]_-{|inFS|}
\\
     |C|
&
     |[A >< (B + C)]|
           \ar[l]^-{|g|}
}
\end{eqnarray*}

\begin{code}
cataFS :: ([(a, Either b c)] -> c) -> FS a b -> c
cataFS g = g . map (recFS (cataFS g)) . outFS
\end{code}

\begin{code}
anaFS :: (c -> [(a, Either b c)]) -> c -> FS a b
anaFS h = inFS . map (recFS (anaFS h)) . h

hyloFS g h = cataFS g . anaFS h
\end{code}

Outras funções pedidas:

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |FS a b|
       \ar[d]_{|check|}
       \ar[r]_-{|outFS|}
&
    |(a >< (b + (FS a b)))^*|
           \ar[d]^{|map (id >< (id + cataFS (g . map (either true id))))|}
           \ar[l]_-{|inFS|}
\\
     |Bool|
&
     |(a >< (b + Bool))^*|
           \ar[l]^-{|g . map (id >< either true id)|}
}
\end{eqnarray*}

\begin{code}
check :: (Eq a) => FS a b -> Bool
check = cataFS (g . map (id >< either true id))
  where
    g :: (Eq a) => [(a, Bool)] -> Bool
    g l =
      let (x, y) = (map fst l, map snd l)
       in length x == length (nub x) && and y
\end{code}

\begin{eqnarray*}
\xymatrix@@C=2cm{
    |FS a b|
       \ar[d]_{|tar|}
       \ar[r]_-{|outFS|}
&
    |(a >< (b + (FS a b)))^*|
           \ar[d]^{|map (id >< (id + tar))|}
           \ar[l]_-{|inFS|}
\\
     |(Path a >< b)^*|
&
     |(a >< (b + ((Path a >< b))^*))^*|
           \ar[l]^-{|concatMap (g . id >< either (singl . split nil id) id)|}
}
\end{eqnarray*}

\begin{code}
tar :: FS a b -> [(Path a, b)]
tar = cataFS (concatMap (g . (id >< either (singl . split nil id) id)))
  where
    g :: (a, [(Path a, b)]) -> [(Path a, b)]
    g (a, lp) = map ((a :) >< id) lp

untar :: (Eq a) => [(Path a, b)] -> FS a b
untar = joinDupDirs . anaFS (map h)
  where
    p = cond (null . tail . p1) (i1 . p2) (i2 . singl . (tail >< id))
    h = split (head . p1) p

find :: Eq a => a -> FS a b -> [Path a]
find a = map fst . filter (f a) . tar
  where
    f :: Eq a => a -> (Path a, b) -> Bool
    f x (y, z) = x == last y

separa :: ([a], b) -> ((a, [a]), b)
separa = split head tail >< id

aposSepara :: ((a, [a]), b) -> [(a, Either b ([a], b))]
aposSepara a =
  singl $
  if null ((p2 . p1) a)
    then split (p1 . p1) (i1 . p2) a
    else split (p1 . p1) (i2 . split (p2 . p1) p2) a

new :: (Eq a) => Path a -> b -> FS a b -> FS a b
new a b = joinDupDirs . concatFS . (,) (anaFS (aposSepara . separa) (a, b))

cp :: (Eq a) => Path a -> Path a -> FS a b -> FS a b
cp [] _ x = x
cp _ [] x = x
cp o d x =
  if p1 possiblePath
    then new d (fromJust (p2 possiblePath)) x
    else x
  where
    findConteudo :: (Eq a) => Path a -> [(Path a, b)] -> (Bool, Maybe b)
    findConteudo origem [] = (False, Nothing)
    findConteudo origem (h:t) =
      if p1 h == origem
        then (True, Just (p2 h))
        else findConteudo origem t
    possiblePath = findConteudo o (tar x)

nav :: Eq a => (Path a, FS a b) -> FS a b
nav = undefined

rm :: (Eq a) => Path a -> FS a b -> FS a b
rm = curry (cond (null . p1) p2 (anaFS h))
  where
    h :: Eq a => (Path a, FS a b) -> [(a, Either b (Path a, FS a b))]
    h =
      cond
        (null . tail . p1)
        (\([x], fs0) -> f [x] (/= x) fs0)
        (\(x:xs, fs0) -> f xs (const True) fs0)
    f :: Path a -> (a -> Bool) -> FS a b -> [(a, Either b (Path a, FS a b))]
    f p b = map (id >< (id -|- split (const p) id)) . filter (b . p1) . outFS

auxJoin :: ([(a, Either b c)], d) -> [(a, Either b (d, c))]
auxJoin = uncurry (flip (map . g))
  where
    g d = id >< (id -|- (,) d)
\end{code}
\subsection*{Valorização}
\begin{code}
cFS2Exp :: a -> FS a b -> Exp () a
cFS2Exp = curry (anaExp h)
  where
    h :: (a, FS a b) -> Either () (a, [(a, FS a b)])
    h = i2 . (id >< map (id >< either id id)) . (id >< map (id >< (const (FS []) -|- id))) .  (id >< outFS)

\end{code}

%----------------- Fim do anexo com soluções dos alunos ------------------------%

%----------------- Índice remissivo (exige makeindex) -------------------------%

\printindex

%----------------- Bibliografia (exige bibtex) --------------------------------%

\bibliographystyle{plain}
\bibliography{cp1819t}

%----------------- Fim do documento -------------------------------------------%
\end{document}
