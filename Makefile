DOCUMENT = cp1819t

.PHONY: lhs pdf tex clean

lhs:
	ghci $(DOCUMENT).lhs

pdf: tex
	latexmk $(DOCUMENT).tex

tex:
	lhs2TeX $(DOCUMENT).lhs > $(DOCUMENT).tex

clean:
	@-latexmk -C
	@-rm $(DOCUMENT).tex *.ptb

