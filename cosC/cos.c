#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define P 3.14159265358979323846

double t(double n) {
    if(n == 0) return 4;
    else return 2 + t(n-1);
}

double s(double n) {
    if(n == 0) return 3;
    else return 2 + s(n - 1);
}


double h(double x, int iterations) {
    if(iterations == 0) return -(x * x) / 2;
    else return h(x, iterations - 1) * (-(x * x / (s(iterations - 1) * t(iterations - 1))));
}


double c(double x, int iterations) {
    if(iterations == 0) return 1;
    else return c(x, iterations - 1) + h(x, iterations - 1);
}

void loop(double *acumuladores, double arg, int iterations) {
    acumuladores[3] = t(acumuladores[3]);
    acumuladores[2] = s(acumuladores[2]);
    acumuladores[1] = h(arg, iterations);
    acumuladores[0] = c(arg, iterations);
}

double reduceAngle(double arg) {
    double redundant = arg / (2 * P);
    double toRemove = floor(redundant);
    double newAngle = 0;
    if(arg > 0) newAngle = arg - (toRemove * (2 * P));
    else newAngle = arg + (toRemove * (2 * P));
    return newAngle;
}

int main(int argc, char **argv) {
    double arg = (double) atof(argv[1]);
    arg = reduceAngle(arg);
    double accumulators[4];
    accumulators[0] = 1;
    accumulators[1] = -(arg * arg) / 2;
    accumulators[2] = 3;
    accumulators[3] = 4;
    loop(accumulators, arg, 10);
    printf("This is the result: %f\n", accumulators[0]);
}

